<?php

/**
 * Description of contacts
 * @author myriam
 */

require_once 'Db.php';
    
class Contacts {
    
    var $contacts;
    var $connection;
    
    function __construct() {
        $this->connection = new Db(); // Should I do this here??
    }
    
    
    function getAllContacts(){
        
        $this->connection = new Db(); // Or here? 
        
        $sql = "SELECT * FROM contacts";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
       
        while($result = $stmt->fetch(PDO::FETCH_ASSOC)){
           $contacts [] = $result;
        }
       
       return $contacts;
    }
    
    function getContact($parameters){
                
        if($parameters['user'] === "all") { $this->getAllContacts(); }
        
        //$params = get diferent params to retrieve from DB and separate them by a comma
        
        $this->connection = new Db();
        
        //$sql = "SELECT params= :params FROM contacts WHERE id= :id";
        $sql = "SELECT * FROM contacts WHERE id= :id";
        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue(':id', $parameters["id"], PDO::PARAM_INT);
        //$stmt->bindValue(':params', $params, PDO::PARAM_STR);
        $stmt->execute();
       
        while($result = $stmt->fetch(PDO::FETCH_ASSOC)){
           $contacts [] = $result;
       }
       
       // I need to close the connection
       
       return $contacts;
    }
    
    function insertContact($input){
        
       $db_columns = array_keys($input); 
       $db_values = array_values($input);

       $set = '';
       for($i=0;$i<count($db_columns);$i++){
            $set .= ($i>0?',':'') . $db_columns[$i] . "=";
            $set .= '"' . $db_values[$i] . '"';    
       }
       
       $this->connection = new Db();
       $sql = "INSERT INTO contacts SET " . $set;
       $stmt = $this->connection->prepare($sql);
       $stmt->execute();
       
    }
}
